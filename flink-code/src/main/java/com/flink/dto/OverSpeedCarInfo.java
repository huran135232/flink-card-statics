package com.flink.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor

public class OverSpeedCarInfo {
    private String areaId;
    private String roadId;
    private String monitorId;
    private String cardNo;
    private Double speed;
    private Double limitSpeed;
    private Date createTime;
}
