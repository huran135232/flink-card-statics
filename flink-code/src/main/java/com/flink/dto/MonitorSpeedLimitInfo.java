package com.flink.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class MonitorSpeedLimitInfo {
    private String areaId;
    private String roadId;
    private String monitorId;
    private Double speedLimit;
}
