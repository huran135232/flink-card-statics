package com.flink.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CardSpeedDTO {
    private String cardNo;
    private String areaId;
    private String roadId;
    private String monitorId;
    private String cameraId;
    private Long actionTime;
    private Double speed;
}
