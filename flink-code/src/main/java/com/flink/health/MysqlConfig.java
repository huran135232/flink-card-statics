package com.flink.health;

/**
 * @author Administrator
 */
public class MysqlConfig {

    public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3356/flink?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8";

    public static final String userName = "root";
    public static final String password = "123456";
}
