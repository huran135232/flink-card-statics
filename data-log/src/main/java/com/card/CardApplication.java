package com.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * @author Administrator
 */
@SpringBootApplication
public class CardApplication {
    public static void main(String[] args) {
        SpringApplication.run(CardApplication.class).registerShutdownHook();

    }
}
