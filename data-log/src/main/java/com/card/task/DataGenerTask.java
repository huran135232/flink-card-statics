package com.card.task;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.random.GaussianRandomGenerator;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 数据模拟生成
 * 1.将数据生成到mysql中
 * 2.将数据生成到Kafka中
 * 区域id,道路id,卡扣id,摄像头id,拍摄时间，车辆信息，车辆速度等维度
 * @author Administrator
 */
@Slf4j
@EnableScheduling
@Configuration
public class DataGenerTask {
    @Scheduled(fixedRate = 5000)
    public void scheduled1() {
        log.info("=====>>>>>使用fixedRate{}", System.currentTimeMillis());
    }

    public static void main(String[] args) throws IOException, ParseException {
        //高斯正态分布
        final GaussianRandomGenerator gaussianRandomGenerator = new GaussianRandomGenerator(new JDKRandomGenerator());

        List<String> plateNumbers = generatePlateNumbers(3000);
        for (String plateNumber : plateNumbers) {
            //模拟每辆车通过的卡口数量，一辆车每天通过卡扣数可能是大部分都不会超过100个卡口
            //abs 大多数属于100个卡口之间
            final double abs = Math.abs(gaussianRandomGenerator.nextNormalizedDouble() * 100);
            for (int i = 0; i < 100; i++) {
                //通过区域
                int areaId = RandomUtil.randomInt(8);
                //通过的道路
                int roadId = RandomUtil.randomInt(50);
                //通过的卡口
                int monitorId = RandomUtil.randomInt(9999);
                //通过的摄像头
                int cameraId=RandomUtil.randomInt(999999);
                final String s = DateUtil.format(new Date(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                //摄像头拍摄时间
                String dateTime = s + " " + RandomUtil.randomInt(10, 24) + ":" + RandomUtil.randomInt(10, 60) + ":" + RandomUtil.randomInt(10, 60);
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long time = sdf.parse(dateTime).getTime();

                //拍摄车辆速度
                int speed= RandomUtil.randomInt(60,100);

                //高斯分布
                final String format = String.format("%s#%d#%d#%d#%d#%s#%d\n", plateNumber, areaId, roadId, monitorId, cameraId, time, speed);

                Files.write(Paths.get("data.txt"), format.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
        }
    }

    public static List<String> generatePlateNumbers(int count) {
        List<String> plateNumbers = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            String provinceCode = generateProvinceCode();
            String cityCode = generateCityCode(provinceCode);
            String serialNumber = generateSerialNumber();

            String plateNumber = provinceCode + cityCode + serialNumber;
            plateNumbers.add(plateNumber);
        }

        return plateNumbers;
    }

    public static String generateProvinceCode() {
        String[] provinceCodes = {"京", "津", "沪", "渝", "冀", "豫", "云", "辽", "黑", "湘", "皖", "鲁", "新", "苏",
                "浙", "赣", "鄂", "桂", "甘", "晋", "蒙", "陕", "吉", "闽", "贵", "粤", "青", "藏", "川", "宁", "琼"};
        Random random = new Random();
        int index = random.nextInt(provinceCodes.length);
        return provinceCodes[index];
    }

    public static String generateCityCode(String provinceCode) {
        String[] cityCodes;

        switch (provinceCode) {
            case "京":
                cityCodes = new String[]{"A"};
                break;
            case "津":
                cityCodes = new String[]{"A", "B"};
                break;
            case "沪":
                cityCodes = new String[]{"A", "B", "C"};
                break;
            case "渝":
                cityCodes = new String[]{"A", "B", "C", "D"};
                break;
            default:
                cityCodes = new String[]{"A", "B", "C", "D", "E", "F", "G"};
                break;
        }

        Random random = new Random();
        int index = random.nextInt(cityCodes.length);
        return cityCodes[index];
    }

    public static String generateSerialNumber() {
        Random random = new Random();
        int serialNumber = random.nextInt(9999);
        return String.format("%04d", serialNumber);
    }
}
